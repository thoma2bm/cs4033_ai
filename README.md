# CS4033_AI

Artificial Intelligence at the University of Cincinnati CS4033 Spring 2019

- [CS4033_AI](#cs4033ai)
  - [FRIL Resources](#fril-resources)
  - [FRIL Notes](#fril-notes)
  - [FRIL Things](#fril-things)

## FRIL Resources

The following may or may not be helpful.

https://en.wikipedia.org/wiki/Fril

https://sorrell.github.io/files/Fril.pdf

## FRIL Notes

to query; start with '?'

- `p` is print
- `pp` is pretty print

## FRIL Things

Show who Jane is the mother of:

```fril
load "../example/family_relations.frl"
?((mother Jane WHO)(pp WHO)(fail))
```

Output:

```fril
Mary
Bill
Trevor
Ann

no
```

`(fail)` allows the search to fail and then continue; this results in a list of all possible people that have Jane as a mother.

Who is Mary's mother:

```fril
load "../example/family_relations.frl"
?((mother WHO Mary)(pp WHO)(fail))
```

Output:

```fril
Jane

no
```

Show everyone and their mother:

```fril
?((mother WHO WHOM)(pp)(p WHO WHOM)(fail))
```

```fril
Jane Mary
Jane Bill
Jane Trevor
Jane Ann
Julie Jane
Monique Tom
no
```